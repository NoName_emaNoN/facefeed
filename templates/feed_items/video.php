<div class="video-block">
	<a class="video-cover-block" href="<?php echo $item->link; ?>" target="_blank">
		<img src="<?php echo $item->picture; ?>" alt="<?php $item->name; ?>" />
		<span></span>
	</a>
	<p><?php echo $item->name; ?></p>
	<p><?php echo $item->caption; ?></p>
</div>