<div class="shared-link-block">
	<?php if (!empty($item->picture)): ?>
	<img src="<?php echo $item->picture; ?>" alt="<?php echo $item->name; ?>" class="shared-link-picture"/>
	<?php endif; ?>
	<a href="<?php echo $item->link; ?>" class="shared-link"><?php echo $item->name; ?></a>

	<p><?php echo $item->caption; ?></p>

	<p><?php echo $item->description; ?></p>
</div>