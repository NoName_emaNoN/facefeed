<div class="feed-block">
	<ul class="feed-list">
		<?php foreach ($data as $item): ?>
		<li>
			<p>
				<a href="http://facebook.com/<?php echo $item->from->id; ?>" class="user-link"><?php echo $item->from->name; ?></a>
				<a href="http://facebook.com/<?php echo $item->id; ?>" target="_blank">Link to post</a>
			</p>

			<?php if (!empty($item->story)): ?>
			<p class="story"><?php echo $item->story; ?></p>
			<?php endif; ?>
			<?php include __DIR__ . '/feed_items/' . $item->type . '.php'; ?>
			<div class="feed-item-footer">
				<?php if (!empty($item->icon)): ?>
				<img src="<?php echo $item->icon; ?>" class="feed-item-type-icon"/>
				<?php endif; ?>
				<span class="likes">Likes (<?php echo !empty($item->likes) ? $item->likes->count : 0; ?>)</span>
				<span class="comments">Comments (<?php echo ($item->comments) ? $item->comments->count : 0; ?>)</span>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>

</div>