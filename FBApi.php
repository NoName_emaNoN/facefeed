<?php
/**
 * FBApi
 *
 * Класс-реализация для работы с Facebook api, посредством Graph API.
 *
 * @author Чемезов Михаил Владимирович <michlenanosoft@gmail.com>
 * @version 1.0
 */

class FBApi
{
	/*
	 * ID приложения, зарегистрированного в Facebook
	 * @link https://developers.facebook.com/apps
	 * @var integer
	 */
	private $app_id = null;

	/*
	 * Секретный код приложения
	 * @link https://developers.facebook.com/apps
	 * @var string
	 */
	private $app_secret = null;

	/*
	 * Нужные приложению права
	 * @var string
	 */
	private $scope = null;

	/*
	 * Access Token, который позволяет совершать запросы к Facebook
	 * @var string
	 */
	private $access_token = null;

	/*
	 * Curl handle. Для работы с запросами
	 */
	private $ch;

	/*
	 * Урл, на который следует перейти после авторизации
	 * @var string
	 */
	public $redirect_url;

	public function __construct($app_id, $app_secret, $scope, $redirect_url)
	{
		$this->app_id = $app_id;
		$this->app_secret = $app_secret;
		$this->scope = $scope;
		$this->redirect_url = $redirect_url;

		if (!session_id())
		{
			session_start();
		}

		$this->access_token = (!empty($_SESSION['access_token'])) ? $_SESSION['access_token'] : null;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLINFO_REDIRECT_COUNT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		$this->ch = $ch;
	}

	/*
	 * Функция возращает состояние авторизации
	 * @return bool
	 */
	public function authorized()
	{
		return (bool)$this->getAccessToken();
	}

	/*
	 * Функция авторизации
	 * @param string $redirect_url
	 * @return bool
	 */
	public function authorize()
	{
		if ($this->authorized()) return true;

		if (empty($_REQUEST['code']))
		{
			$this->redirect($this->getLoginUrl());
		}

		if ($this->checkState($_REQUEST['state']))
		{
			if (isset($_REQUEST['error']))
			{
				throw new ErrorException(isset($_REQUEST['error_description']) ? $_REQUEST['error_description'] : 'Ошибочка вышла...');
			}

			curl_setopt($this->ch, CURLOPT_URL, $this->getTokenUrl($_REQUEST['code']));

			$response = curl_exec($this->ch);

			if ($response === false)
			{
				throw new ErrorException('Не удалось выполнить запрос');
			}

			parse_str($response, $params);

			$this->setAccessToken($params['access_token']);

			$this->redirect($this->redirect_url);
		}
		else
		{
			/* Что-то не так с параметром state, перенаправим в начало */
			$this->redirect($this->redirect_url);
		}
	}

	/*
	 * Возвращает state если он сгенерирован, если нет - генерирует
	 * @link http://en.wikipedia.org/wiki/Cross-site_request_forgery
	 * @return string
	 */
	public function getState()
	{
		if (empty($_SESSION['state']))
		{
			$_SESSION['state'] = md5(uniqid(rand(), true));
		}

		return $_SESSION['state'];
	}

	/*
	 * Проверяет state на соответствие тому, что в сессии хранится
	 * @return bool
	 */
	public function checkState($state)
	{
		return ($state == $this->getState());
	}

	/*
	 * Возвращает урл для окна логина
	 * @return string
	 */
	private function getLoginUrl()
	{
		return 'http://www.facebook.com/dialog/oauth/?
		    client_id=' . $this->app_id . '
		    &redirect_uri=' . $this->redirect_url . '
		    &scope=' . $this->scope . '
		    &state=' . $this->getState();
	}

	private function getTokenUrl($code)
	{
		return 'https://graph.facebook.com/oauth/access_token?'
			. 'client_id=' . $this->app_id . '&redirect_uri=' . $this->redirect_url
			. '&client_secret=' . $this->app_secret . '&code=' . $code;
	}

	/*
	 * Возвращает access_token
	 * @return string
	 */
	public function getAccessToken()
	{
		return $this->access_token;
	}

	/*
	 * Устанавливает access_token
	 * @param string $access_token
	 */
	private function setAccessToken($access_token)
	{
		$this->access_token = $access_token;
		$_SESSION['access_token'] = $access_token;
	}

	/*
	 * Функция перенаправляет приложение на нужный урл
	 * @param string $url URL для перенаправления
	 */
	protected function redirect($url)
	{
		header('Location: ' . $url);
		die();
	}

	/*
	 * Главный метод в классе, делает запрос и возвращает данные
	 * @param string $query
	 * @return array
	 */
	public function api($query)
	{
		curl_setopt($this->ch, CURLOPT_URL, 'https://graph.facebook.com' . $query . '?access_token=' . $this->getAccessToken());
		$response = curl_exec($this->ch);

		if ($response === false)
		{
			throw new ErrorException('Не удалось выполнить запрос');
		}

		return json_decode($response);
	}

}
