<?php

$app_id = "319147264861973";
$app_secret = "32df6a154f5be2a12e578ae75e38f199";
$my_url = "http://nanoteam.net/facefeed/";

require_once 'FBApi.php';

$fb = new FBApi($app_id, $app_secret, 'read_stream', $my_url);
$fb->authorize();

$response = $fb->api('/me/home');

$data = $response->data;
$paging = $response->paging;

ob_start();
include __DIR__ . '/templates/index.php';
$content = ob_get_clean();
include __DIR__ . '/templates/layout.php';

?>